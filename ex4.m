syms v_1 v_2 v_3 x_1 x_2 x_3 real;
v = [v_1 v_2 v_3]';
x = [x_1 x_2 x_3]';

vXx = cross(v, x);

latex(v)
latex(x)
latex(vXx)
