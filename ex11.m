theta_x = pi/2;
theta_y = pi/2;

rx = rotx(theta_x)
ry = roty(theta_y)

R01 = rx*ry

hold off
trplot([eye(3) [0 0 0]'; [0 0 0 1]], 'rgb', 'frame', '0');
hold on
trplot([R01 [1.2 1.2 0]'; [0 0 0 1]], 'rgb', 'frame', '1');
hold off

axis([-1 2.5 -1 2.5 -1 1.2])

export_fig('latex/figs/ex11', '-pdf', '-painters', '-transparent');