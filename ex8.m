clear;

syms real;

w = (sqrt(3)/3)*[1 -1 1]';
hat_w = hat(w);

theta = 30*pi/180;
R = eye(3) + sin(theta)*hat_w + (1 - cos(theta))*hat_w^2
latex(sym(R))

R2 = expm(hat_w*theta)