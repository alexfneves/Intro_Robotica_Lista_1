clear;

syms v_1 v_2 v_3 w_1 w_2 w_3 lambda real;
v = [v_1 v_2 v_3]';
w = [w_1 w_2 w_3]';

hat_w = hat(w);

det(hat_w - lambda*eye(3))

[solv1, solv2, solv3] = solve(hat_w*v == 0, [v_1, v_2, v_3])