clear;

syms x y z real;

% a = [0.433 x 0.866]';
% b = [-0.5 0.866 y]';
% c = [z -0.433 0.5]';
% 
% R = [a b c];
% 
% eq = [a'*b; a'*c; b'*c];
% 
% A = [diff(eq,x) diff(eq,y) diff(eq,z)];
% 
% B = - eq + A*[x y z]';
% 
% % A[x y z]' = b
% 
% A\B

R = [0.433 -0.5 z; x 0.866 -0.433; 0.866 y 0.5];
rrt = R*R';
eq = rrt - eye(3);
mod_x = vpa(sqrt(x^2 - eq(2,2)));
mod_y = vpa(sqrt(y^2 - eq(3,3)));
mod_z = vpa(sqrt(z^2 - eq(1,1)));

Rsub = vpa(subs(R, [x, y, z], [mod_x, mod_y, -mod_z]));
det(Rsub)