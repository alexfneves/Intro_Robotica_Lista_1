clear;

Tp_y = [eye(3) [0 1 0]'; 0 0 0 1]
T_R_z = [rotz(pi/2) [0 0 0]'; 0 0 0 1]
T_p_x = [eye(3) [3 0 0]'; 0 0 0 1]

TIb = Tp_y*T_p_x*T_R_z;

hold off
trplot(eye(4), 'rgb', 'frame', 'I');
hold on
trplot(TIb, 'rgb', 'frame', 'b');
hold off

axis([-1 4 -1 2 -1 2])

export_fig('latex/figs/ex13', '-pdf', '-painters', '-transparent');